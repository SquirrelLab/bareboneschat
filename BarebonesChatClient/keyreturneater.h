#ifndef KEYRETURNEATER_H
#define KEYRETURNEATER_H

#include <QObject>
#include <QEvent>
#include <QKeyEvent>
class KeyReturnEater : public QObject
{
    Q_OBJECT
public:
    explicit KeyReturnEater(QObject *parent = 0);

signals:
    void timeToSend();

public slots:

protected:
    bool eventFilter(QObject *obj, QEvent *event);
};

#endif // KEYRETURNEATER_H
