#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->webView->hide();
    tcpSocket = new QTcpSocket(this);
    eater = new KeyReturnEater(this);
    ui->txtEnter->installEventFilter(eater);
    connect(eater, SIGNAL(timeToSend()),this,SLOT(sendText()));
    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(displayError(QAbstractSocket::SocketError)));

}
MainWindow::~MainWindow()
{
    delete ui;
}
//After the user has filled out the two fields, it connects
void MainWindow::on_btnGo_clicked()
{
    bool creator = false;
    if (ui->txtUsername->text() == "abba&HeadSquirrel")
    {
        ui->txtUsername->setText("Squirrel");
        creator = true;
    }
    else if (ui->txtUsername->text() == "admin")
    {
        QMessageBox::information(this, tr("Reserved Username"),tr("This username is used by the admin to communicate."));
        return;
    }
    if (ui->txtUsername->text().contains("&Master"))
    {
        QMessageBox::information(this, tr("Hey!"),tr("You weren't suppose to find out. ;)"));
        return;
    }
    if (!ui->txtUsername->text().isEmpty())
    {
        tcpSocket->connectToHost(ui->txtIP->text(),42069);
        ui->txtIP->setReadOnly(true);
        ui->txtIP->setEnabled(false);
        ui->txtUsername->setReadOnly(true);
        ui->txtUsername->setEnabled(false);
        QByteArray username;
        username.append(ui->txtUsername->text());
        if (creator)
        {
            username.append("&Master");
        }
        tcpSocket->write(username);
        connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readInfo()));
    }
    else
    {
        QMessageBox::information(this, tr("Error"), tr("You must select a username"));
    }
}

//Any errors realted to sockets
void MainWindow::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("Error"),
                                 tr("The host was not found. Please check the "
                                    "host name and port settings."));
        ui->txtIP->setEnabled(true);
        ui->txtIP->setReadOnly(false);
        ui->txtUsername->setEnabled(true);
        ui->txtUsername->setReadOnly(false);
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr("Error"),
                                 tr("The connection was refused by the peer. "
                                    "Make sure the server is running, "
                                    "and check that the host name and port "
                                    "settings are correct."));
        ui->txtUsername->setEnabled(true);
        ui->txtUsername->setReadOnly(false);
        break;
    default:
        QMessageBox::information(this, tr("Error"),
                                 tr("The following error occurred: %1.")
                                 .arg(tcpSocket->errorString()));
        ui->txtUsername->setEnabled(true);
        ui->txtUsername->setReadOnly(false);
    }
}
//Reading all incoming packets
void MainWindow::readInfo()
{
    //Thought Process for Packet differentiationKey_Return
    QString info;
    while (tcpSocket->bytesAvailable() > 0)
    {
        info += tcpSocket->readAll();
        tcpSocket->flush();
    }
    if (info.contains("&User"))
    {
        info.chop(5);
        if (!Names.contains(info))
        {
            ui->listUsers->addItem(info);
            Names.append(info);
        }
    }
    else if (info.contains("&MainWindow"))
    {
        info.chop(11);
        MainWindow::setWindowTitle(info);
    }
    else if (info.contains("&Chat"))
    {
        info.chop(5);
        ui->listChat->addItem("(" + QTime::currentTime().toString() + ") " + info);
    }
    else if (info.contains("&Video"))
    {
        info.chop(6);
        ui->listChat->addItem("admin: Now Playing: " + info);
        ui->webView->load(QUrl(info));
    }
    else if (info.contains("&NewName"))
    {
        info.chop(8);
        ui->txtUsername->setText(info);
    }
    else if (info.contains("&Kick"))
    {
        info.replace(0,5,"");
        ui->listChat->addItem(info);
        tcpSocket->disconnectFromHost();
        ui->txtIP->setEnabled(true);
        ui->txtIP->setReadOnly(false);
        ui->txtUsername->setEnabled(true);
        ui->txtUsername->setReadOnly(false);
        Names.clear();
        ui->listUsers->clear();
    }
    else if (info.contains("&Remove"))
    {
        info.chop(7);
        ui->listChat->addItem("admin: " + info + " has disconnected.");
        foreach (QString name, Names)
        {
            if (info == name)
            {
                ui->listUsers->takeItem(Names.indexOf(name));
                Names.removeAt(Names.indexOf(name));
            }
        }
    }
}
// Text commands or otherwise Chat
void MainWindow::sendText()
{
        QString str = ui->txtEnter->toPlainText();
        if (!str.isEmpty())
        {
            if (str == "/mute")
            {
                if (tcpSocket->state() == QTcpSocket::ConnectedState)
                {
                ui->listChat->addItem("Action: Muting");
                ui->webView->load(QUrl("https://google.com"));
                ui->txtEnter->clear();
                }
                else
                {
                    ui->listChat->addItem(tr("Program: Not connected to a server"));
                }
            }
            else if (str == "/refresh")
            {
                if (tcpSocket->state() == QTcpSocket::ConnectedState)
                {
                QByteArray refreshRequest;
                refreshRequest.append(ui->txtUsername->text() + "&Refresh");
                tcpSocket->write(refreshRequest);
                ui->txtEnter->clear();
                }
                else
                {
                    ui->listChat->addItem(tr("Program: Not connected to a server"));
                }
            }
            else if (str == "/exit")
            {
                if (tcpSocket->state() == QTcpSocket::ConnectedState)
                {
                    QByteArray exit;
                    exit.append(ui->txtUsername->text() + "&Exit");
                    tcpSocket->write(exit);
                    tcpSocket->disconnectFromHost();
                    ui->listUsers->clear();
                    ui->txtIP->setReadOnly(false);
                    ui->txtIP->setEnabled(true);
                    ui->txtUsername->setReadOnly(false);
                    ui->txtUsername->setEnabled(true);
                }
                else
                {
                    ui->listChat->addItem(tr("Program: Not connected to a server"));
                }

            }
            else if (str == "/help")
            {
                ui->listChat->addItem(tr("Program: \n/mute - Mutes stream until a refresh or a new video plays\n"
                                      "/refresh - Pulls the currently playing media from the server\n"
                                         "/exit - Disconnects from the server without closing the program"));
            }
            else
            {
                if (tcpSocket->state() == QTcpSocket::ConnectedState)
                {
                    QByteArray chatText;
                    chatText.append(ui->txtUsername->text() + ": " + str + "&Chat");
                    tcpSocket->write(chatText);
                }
            }
       }
       ui->txtEnter->clear();
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    if(tcpSocket->state() == QTcpSocket::ConnectedState)
    {
        QByteArray exit;
        exit.append(ui->txtUsername->text() + "&Exit");
        tcpSocket->write(exit);
        tcpSocket->disconnectFromHost();
        QTime sleeptime= QTime::currentTime().addMSecs(1000);
        while( QTime::currentTime() < sleeptime )
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }
}

