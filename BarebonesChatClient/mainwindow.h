#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QMessageBox>
#include <QDataStream>
#include <keyreturneater.h>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void displayError(QAbstractSocket::SocketError socketError);

    void on_btnGo_clicked();

    void readInfo();

    void sendText();

    void closeEvent(QCloseEvent *e);


private:
    Ui::MainWindow *ui;
    QTcpSocket *tcpSocket;
    QList<QString> Names;
    KeyReturnEater *eater;
};

#endif // MAINWINDOW_H
