#include "keyreturneater.h"

KeyReturnEater::KeyReturnEater(QObject *parent) : QObject(parent)
{

}

bool KeyReturnEater::eventFilter(QObject *obj, QEvent * event)
{
    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent * keyEvent = static_cast<QKeyEvent *>(event);
        if (keyEvent->key() == Qt::Key_Return && keyEvent->modifiers() != Qt::SHIFT)
        {
            emit timeToSend();
            return true;
        }
        else
        {
            return QObject::eventFilter(obj,event);
        }
    }
}
