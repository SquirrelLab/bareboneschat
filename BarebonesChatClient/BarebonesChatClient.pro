#-------------------------------------------------
#
# Project created by QtCreator 2015-04-28T20:07:22
#
#-------------------------------------------------

QT       += core gui network webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets multimedia network

TARGET = BarebonesChatClient
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    keyreturneater.cpp

HEADERS  += mainwindow.h \
    keyreturneater.h

FORMS    += mainwindow.ui
