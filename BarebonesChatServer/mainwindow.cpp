#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    removeToolBar(ui->mainToolBar);
    time = new QElapsedTimer();
    repeat = 0;
    eater = new KeyReturnEater(this);
    ui->txtEnter->installEventFilter(eater);
    connect(eater,SIGNAL(timeToSend()),this,SLOT(textAction()));
    tcpServer = new QTcpServer(this);
    if (!tcpServer->listen(QHostAddress::Any,42069))
        ui->listChat->addItem(tr("Program: Unable to forward needed port. Do you have something else using it?"));
    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(addUser()));
    ui->lblChat->setText(tr("Chat:"));
    ui->lblGuests->setText(tr("Guests:"));
    ui->lblPlaylist->setText(tr("Web View:"));
    ui->lblTitle->setText(tr("Title for Server:"));
    ui->listChat->addItem(tr("Program: Successfully obtained port ") + QString::number(tcpServer->serverPort()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_lineTitle_editingFinished()
{
    MainWindow::setWindowTitle(ui->lineTitle->text());
    foreach (QTcpSocket *client, clientConnections)
    {
        QByteArray array;
        array.append(ui->lineTitle->text() + "&MainWindow");
        client->write(array);
    }
}



//Will only activate when the server has recieved connections
void MainWindow::addUser()
{
    bool master = false;
    QMessageBox::information(this,tr("A New User Has Arrived!"),tr("They will be added to the list."));
    QTcpSocket *clientConnection = tcpServer->nextPendingConnection();
    connect(clientConnection, SIGNAL(disconnected()), clientConnection, SLOT(deleteLater()));
    clientConnections.append(clientConnection);
    QString username = clientConnection->readAll();
    if (!username.isEmpty())
    {
         if (username.contains("&Master"))
         {
            username.chop(7);
            master = true;
         }
         if (users.contains(username))
         {
             repeat++;
             ui->listGuests->addItem(username + QString::number(repeat));
             if(!master)
                users.append(username + QString::number(repeat));
             else if (master)
                 users.append("abbabaab" + username + QString::number(repeat));
             QByteArray newName;
             newName.append(username + QString::number(repeat) + "&NewName");
             clientConnection->write(newName);
         }
         else
         {
             ui->listGuests->addItem(username);
             users.append(username);
         }
    }
    else
        ui->listGuests->addItem("No Username");

    foreach (QTcpSocket *client, clientConnections)
    {
        foreach (QString user, users)
        {
            QByteArray array;
            array.append(user + "&User");
            client->write(array);
            QTime sleeptime= QTime::currentTime().addMSecs(100);
            while( QTime::currentTime() < sleeptime )
                QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
        }
    }
    threadedread *thread = new threadedread(clientConnection->socketDescriptor(),clientConnection,this);
    connect(thread, SIGNAL(chatFound(QString)), this, SLOT(onChatRecieved(QString)));
    connect(thread, SIGNAL(finished()), thread,SLOT(disconnect()));
    thread->start();
    QByteArray forNewConnection;
    forNewConnection.append(ui->lineTitle->text() + "&MainWindow");
    clientConnection->write(forNewConnection);
    forNewConnection.clear();
    QTime sleeptime= QTime::currentTime().addMSecs(50);
    while( QTime::currentTime() < sleeptime )
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    if (!ui->txtAdd->text().isEmpty())
    {
        forNewConnection.append(ui->txtAdd->text() + "&start=" + QString::number(time->elapsed()/1000-1) + "&Video");
        clientConnection->write(forNewConnection);
        forNewConnection.clear();
    }
}

void MainWindow::onChatRecieved(QString data)
{
    if (data.contains("&Refresh"))
    {
        foreach (QString user, users)
        {
            if (data == user + "&Refresh")
            {
                QTcpSocket *currentClient = clientConnections[users.indexOf(user)];
                QByteArray array;
                array.append(ui->txtAdd->text() + "&start=" + QString::number(time->elapsed()/1000-1) + "&Video");
                currentClient->write(array);
            }
        }
    }
    else if (data.contains("&Exit"))
    {
        foreach (QString user, users)
        {
            if (data == user + "&Exit")
            {
                ui->listChat->addItem(tr("Program: Disconnecting: ") + user);
                QTcpSocket *current = clientConnections[users.indexOf(user)];
                current->disconnectFromHost();
                clientConnections.removeAt(users.indexOf(user));
                foreach(QTcpSocket *client, clientConnections)
                {
                    QByteArray array;
                    array.append(user + "&Remove");
                    client->write(array);
                }
                ui->listGuests->takeItem(users.indexOf(user));
                users.removeAt(users.indexOf(user));
            }
        }
    }
    else
    {
        data.chop(5);
        ui->listChat->addItem("(" + QTime::currentTime().toString() + ") " + data);
        foreach(QTcpSocket *client, clientConnections)
        {
                QByteArray array;
                array.append(data + "&Chat");
                client->write(array);
        }
    }
}

void MainWindow::on_btnPlay_clicked()
{
    if(!ui->txtAdd->text().isEmpty() && ui->txtAdd->text().contains("youtube.com"))
    {
        ui->listChat->addItem(QString::number(time->elapsed()/1000));
        QString fixed = ui->txtAdd->text();
        QString ID = fixed.mid(fixed.length() - 11, fixed.length());
        fixed.chop(19);
        fixed += "embed/" + ID + "?autoplay=true";
        ui->txtAdd->setText(fixed);
        foreach (QTcpSocket *client, clientConnections)
        {
            QByteArray array;
            array.append(fixed + "&Video");
            client->write(array);
        }
        ui->webView->load(QUrl(fixed));
        time->start();
    }
    else if(ui->txtAdd->text().contains("soundcloud.com"))
    {
        QMessageBox::information(this,tr("Soundcloud Advisory"),tr("Soundcloud does not have a way to disable autoplay of the recommended"
                                                                   " tracks. Idle attention to the current media could result in the possibility"
                                                                   " of unwanted tracks being played."));
        ui->listChat->addItem(QString::number(time->elapsed()/1000));
        QString fixed = ui->txtAdd->text();
        //QString ID = fixed.mid(fixed.length() - 11, fixed.length());
        //fixed.chop(19);
        fixed += "#play";
        ui->txtAdd->setText(fixed);
        foreach (QTcpSocket *client, clientConnections)
        {
            QByteArray array;
            array.append(fixed + "&Video");
            client->write(array);
        }
        ui->webView->load(QUrl(fixed));
        time->start();
    }
    else
    {
        QMessageBox::warning(this, tr("Information Not Valid"), tr("You must input a proper link either from youtube or soundcloud."));
    }
}

void MainWindow::textAction()
{
    QString str = ui->txtEnter->toPlainText();
    if (str.contains("/kick"))
    {
        bool valid = false;
        foreach (QString user, users)
        {
            if (str.contains(user))
            {
                QTcpSocket *currentClient = clientConnections[users.indexOf(user)];
                currentClient->write("&Kickadmin: You have been kicked.");
                currentClient->disconnectFromHost();
                clientConnections.removeAt(users.indexOf(user));
                ui->listGuests->takeItem(users.indexOf(user));
                users.removeAt(user.indexOf(user));
                valid = true;
                foreach(QTcpSocket *client, clientConnections)
                {
                    QByteArray array;
                    array.append(user + "&Remove");
                    client->write(array);
                }
            }
        }
        if (!valid)
        {
            ui->listChat->addItem("Program: User does not exist");
        }
    }
    else if (str.contains("/help"))
    {
        ui->listChat->addItem(tr("Program: \n/kick - This kicks a user from the server. \nFormatted like \"/kick foo\""));
    }
    else
    {
        ui->listChat->addItem("(" + QTime::currentTime().toString() + ")" + " admin: " + str);
        foreach(QTcpSocket *client, clientConnections)
        {
                QByteArray array;
                array.append("admin: " + str + "&Chat");
                client->write(array);
        }
    }
    ui->txtEnter->clear();
}
