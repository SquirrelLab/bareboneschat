#ifndef THREADEDREAD_H
#define THREADEDREAD_H

#include <QObject>
#include <QThread>
#include <QtNetwork>
#include <QDataStream>
class threadedread : public QThread
{
    Q_OBJECT
public:
    threadedread(int socketDescriptor,QTcpSocket *parentsocket, QObject *parent);

    void run();

signals:
    void chatFound(QString info);

public slots:
    void handleRead();
    void disconnect();
private:
        QTcpSocket *socket;
        int description;
        qint16 blockSize;
};




#endif // THREADEDREAD_H
