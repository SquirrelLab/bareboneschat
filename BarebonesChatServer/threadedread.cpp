#include "threadedread.h"

threadedread::threadedread(int socketDescriptor, QTcpSocket *parentsocket, QObject *parent) : QThread(parent)
{
    this->description = socketDescriptor;
    socket = parentsocket;
}

void threadedread::run()
{
    socket = new QTcpSocket();
    socket->setSocketDescriptor(description);
    connect(socket, SIGNAL(readyRead()), this, SLOT(handleRead()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnect()));
    exec();
}

void threadedread::handleRead()
{
    QString info;
    while(socket->bytesAvailable() > 0)
    {
        info += socket->readAll();
        socket->flush();
    }
    emit chatFound(info);
}
void threadedread::disconnect()
{
    socket->deleteLater();
    exit(0);
}
