#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlaylist>
#include <QMessageBox>
#include <QtNetwork>
#include <QElapsedTimer>
#include <QWebView>
#include <threadedread.h>
#include <keyreturneater.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected slots:

    void addUser();

    void on_lineTitle_editingFinished();

    void onChatRecieved(QString data);

    void textAction();

private slots:
    void on_btnPlay_clicked();

private:
    Ui::MainWindow *ui;
    QTcpServer *tcpServer;
    QList<QTcpSocket *> clientConnections;
    QList<QString> users;
    int repeat;
    QElapsedTimer *time;
    KeyReturnEater *eater;
};

#endif // MAINWINDOW_H
