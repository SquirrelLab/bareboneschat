#-------------------------------------------------
#
# Project created by QtCreator 2015-04-22T15:14:03
#
#-------------------------------------------------

QT       += core gui multimedia network webkitwidgets
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BarebonesChatServer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    threadedread.cpp \
    keyreturneater.cpp

HEADERS  += mainwindow.h \
    threadedread.h \
    keyreturneater.h

FORMS    += mainwindow.ui
